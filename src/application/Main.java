package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entities.Files;
import entities.Product;

public class Main {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Product> product = new ArrayList<>();
		
		System.out.print("Enter the path for folder create: ");
		String path = sc.nextLine();
		System.out.print("Enter the name for folder create: ");
		String nameFolder = sc.nextLine();
		Files.createFolder(path, nameFolder);
		
		
		
		System.out.print("Enter the number of products you wanna scribe: ");
		int n = sc.nextInt();
		
		System.out.println("Products: ");
		
		for (int i = 1; i <= n; i++) {
			sc.nextLine();
			System.out.println("Product #"+i+": ");
			System.out.print("Name: ");
			String name = sc.nextLine();
			System.out.print("Price: ");
			double price = sc.nextDouble();
			System.out.print("Quantity: ");
			int qtd = sc.nextInt();
			
			product.add(new Product(name, price, qtd));
		}
		
		System.out.println("\nWait a minute... scribing on file: ");
		
		String newPath = path+nameFolder;
		for (Product p : product) {
			Files.writeFile(newPath, "\\initial.csv" ,p.escrever());
		}
		
		Files.createFolder(newPath, "\\out");
		
		Files.readFile(newPath);
		
		
		
		
		
		
		
		sc.close();
	}

}
