package entities;

public class Product {
	private String name;
	private double preco;
	private int quantity;
	
	public Product() {
		
	}

	public Product(String name, double preco, int quantity) {
		this.name = name;
		this.preco = preco;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String escrever() {
		return getName()+","+String.format("%.2f", getPreco())+","+getQuantity();
	}
	
}
