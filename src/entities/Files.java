package entities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Files {

	public static void writeFile(String path, String nameFile, String product) {
		String newPath = path + nameFile;
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(newPath, true))) {
			bw.write(product);
			bw.newLine();

		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public static void readFile(String path) {
		String newPath = path + "\\initial.csv";

		try (BufferedReader br = new BufferedReader(new FileReader(newPath))) {
			String line = br.readLine();
			while (line != null) {
				String[] lineTemp = line.split(",");
				String name = lineTemp[0];
				Double price = Double.parseDouble(lineTemp[1]);
				int qtd = Integer.parseInt(lineTemp[2]);
				writeFile(path, "\\out\\summary.csv", name + "," + price * qtd);
				line = br.readLine();

			}
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public static void createFolder(String path, String name) {
		new File(path, "\\" + name).mkdir();
	}
}
